<?php

namespace App\Policies;

use App\User;
use App\Ad;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Can user delete ad
     *
     * @param  User  $user
     * @param  Task  $task
     * @return bool
     */
    public function edit(User $user, Ad $ad)
    {
        return $ad->user_id === $user->id;
    }
}
