<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Ad extends Model
{
    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getUser()
    {
        return $this->belongsTo(User::class);
    }

    public function getUserName()
    {
        $user = User::where(['id' => $this->user_id])->first()->username;

        return $user;
    }
}

