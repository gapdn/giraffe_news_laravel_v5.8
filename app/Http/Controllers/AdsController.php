<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Repositories\AdRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdsController extends Controller
{

    /**
     * @var AdRepository
     */
    protected $ads;

    /**
     * AdsController constructor.
     * @param AdRepository $ads
     */
    public function __construct(AdRepository $ads)
    {
        $this->ads = $ads;
        return $this->middleware('auth')->except(['index', 'show', 'about']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('home', [
            'ads' => $this->ads->allAds() ?? '',
        ]);
    }

    /**
     * Display an about page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('about');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        return view('ads.create', [
            'action' => $action,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3|max:100',
            'description' => 'required|max:1000',
        ]);

        $request->user()->ads()->create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $request->user()->id,
        ]);

        $id = DB::getPdo()->lastInsertId();

        session()->flash('success', 'Successfully created ad!');

        return redirect('/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        return view('ads.show', [
            'ad' => $ad,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        $this->authorize('edit', $ad);
        $action = 'update';
        return view('ads.edit', [
            'action' => $action,
            'ad' => $ad
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $this->authorize('edit', $ad);
        $this->validate($request, [
            'title' => 'required|min:3|max:100',
            'description' => 'required|max:1000',
        ]);

        $ad->update([
            'title' => $request->title,
            'description' => $request->description,
        ]);
        session()->flash('success', 'Ad updated successfully');
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $this->authorize('edit', $ad);

        if($ad->delete()) {
            session()->flash('success', 'Ad deleted');
        }

        return redirect('/');
    }
}
