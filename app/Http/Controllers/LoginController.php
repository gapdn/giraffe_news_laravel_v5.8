<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    use RegistersUsers;

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|min:3|max:13|alpha_dash',
            'password' => 'required|min:6|alpha_dash',
        ]);
        $credentials = $request->only('username', 'password');

        if (!DB::table('users')->where('username', $credentials['username'])->first()) {
            $this->guard()->login($this->create($credentials));
        } elseif (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        }

        throw ValidationException::withMessages([
            'password' => [trans('auth.failed')],
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'password' => Hash::make($data['password']),

        ]);
    }

    public function username()
    {
        return 'username';
    }
}