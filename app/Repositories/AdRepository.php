<?php


namespace App\Repositories;
use App\Ad;

class AdRepository
{
    /**
     * Count of pages for pagination
     */
    const COUNT_PAGES = 5;

    /**
     * Get all ads
     *
     * @param  Ad  $ad
     * @return Collection
     */
    public function allAds()
    {
        return Ad::orderBy('id', 'desc')->paginate(self::COUNT_PAGES);
    }
}