<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::get('/', 'AdsController@index')->name('home');
Route::post('/store', 'AdsController@store')->name('create');
Route::get('/delete/{ad}', 'AdsController@destroy')->name('delete');
Route::post('/login', 'LoginController@login')->name('login');
Route::get('/edit/{ad}', 'AdsController@edit')->name('edit');
Route::get('/edit','AdsController@create');
Route::get('/about', 'AdsController@about');
Route::get('/{ad}', 'AdsController@show')->name('show');
Route::put('/update/{ad}', 'AdsController@update')->name('update');
