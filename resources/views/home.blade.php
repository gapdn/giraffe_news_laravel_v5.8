@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @guest
                <div class="card">
                    <div class="card-header">Login</div>
                    <div class="card-body">
                        @include('layouts.login_form')
                    </div>
                </div>
                <br>
            @endguest
            <div class="card">
                <div class="card-header">News</div>
                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-block" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('success') }}
                        </div>
                    @elseif(session('warning'))
                        <div class="alert alert-warning" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('warning') }}
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    @if($ads)
                        @foreach($ads as $ad)
                            <div><a href="{{ route('show', ['id' => $ad->id])  }}">{{ $ad->title  }}</a></div>
                            <div>{{ $ad->description  }}</div>

                            <div><b>Created: </b>{{ $ad->created_at  }}<b> by </b>{{ $ad->getUserName() }}</div>
                            @if(request()->user() && request()->user()->id == $ad->user_id)

                                <div class="row">
                                    <div class="col-sm-1">
                                        <form action="{{ route('delete', ['id' => $ad->id]) }}" method="POST">
                                            @csrf
                                            @method('GET')
                                            <button type="submit" id="delete-ad-{{ $ad->id }}" class="btn btn-sm btn-outline-danger">Delete</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-1">
                                        <form action="{{ url('edit/'.$ad->id) }}" method="GET">
                                            @method('GET')
                                            <button type="submit" id="edit-ad-{{ $ad->id }}" class="btn btn-sm btn-outline-info">Edit</button>
                                        </form>
                                    </div>
                                </div>


                            @endif
                            <hr>
                        @endforeach
                        {{ $ads->links() }}
                    @else
                        <div>NObody posted yet</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
