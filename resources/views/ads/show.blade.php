@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ad</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('success') }}
                        </div>
                    @elseif(session('warning'))
                        <div class="alert alert-warning" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('warning') }}
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    <h3>{{ $ad->title  }}</h3>
                    <div>{{ $ad->description  }}</div>
                    <div><b>Created at: </b>{{ $ad->created_at  }}</div>
                    <div><b>Author: </b>{{ $ad->getUserName() }}</div>
                    @if(request()->user() && request()->user()->id == $ad->user_id)
                    <div class="row">
                        <div class="col-sm-1">
                            <form action="{{ url('delete/'.$ad->id) }}" method="POST">
                                @csrf
                                @method('GET')
                                <button type="submit" id="delete-ad-{{ $ad->id }}" class="btn btn-sm btn-outline-danger">Delete</button>
                            </form>
                        </div>
                        <div class="col-sm-1">
                            <form class="pull-right" action="{{ url('edit/'.$ad->id) }}" method="GET">
                                @method('GET')
                                <button type="submit" id="edit-ad-{{ $ad->id }}" class="btn btn-sm btn-outline-info">Edit</button>
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
